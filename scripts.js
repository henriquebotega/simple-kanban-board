// script
const cards = document.querySelectorAll(".card");
const dropzones = document.querySelectorAll(".dropzone");

function dragstart() {
	dropzones.forEach((dz) => dz.classList.add("highlight"));
	this.classList.add("is-dragging");
}

function dragend() {
	dropzones.forEach((dz) => dz.classList.remove("highlight"));
	this.classList.remove("is-dragging");
}

cards.forEach((card) => {
	card.addEventListener("dragstart", dragstart);
	card.addEventListener("drag", () => {});
	card.addEventListener("dragend", dragend);
});

function dragover() {
	this.classList.add("is-over");
	const cardBeingDragged = document.querySelector(".is-dragging");
	this.appendChild(cardBeingDragged);
}

function dragleave() {
	this.classList.remove("is-over");
}

dropzones.forEach((dz) => {
	dz.addEventListener("dragenter", () => {});
	dz.addEventListener("dragover", dragover);
	dz.addEventListener("dragleave", dragleave);
	dz.addEventListener("drop", () => {});
});
